//
//  LoginViewController.swift
//  British
//
//  Created by MirkoPinna on 12/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD
import FacebookCore
import FacebookLogin
import Firebase

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailET: UITextField!
    @IBOutlet weak var passwET: UITextField!
    
    var segueUser:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let user = UserPersistence().loadUser()
        if(user != nil) {
            //Go to main page
            self.performSegue(withIdentifier: "HomeSegue", sender: nil)
        } else {
            let fbUser = UserPersistence().loadFBUser()
            if(fbUser != nil) {
                //Go to main page
                self.performSegue(withIdentifier: "HomeSegue", sender: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToLogin(_ sender:UIStoryboardSegue){}
    
    @IBAction func didTapFBBtn(_ sender: UIButton) {
        let login = LoginManager()
        
        login.logIn([.publicProfile, .email], viewController: self) {
            loginResult in
            
            switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("Logged in!")
                    if(!accessToken.authenticationToken.isEmpty) {
                        SVProgressHUD.show()
                        NetworkManager.sharedInstance.loginWithFB(token: accessToken.userId!, success: { (user) in
                            SVProgressHUD.dismiss()
                            if(user != nil) {
                                UserPersistence().persistFBUser(user!)
                                if let token = FIRInstanceID.instanceID().token() {
                                    self.sendFCMTokenToServer(token: token)
                                } else {
                                    //Go Home
                                    self.performSegue(withIdentifier: "HomeSegue", sender: nil)
                                }
                                
                            } else {
                                //Needs registration
                                let fbSegueUser = FBUser()
                                fbSegueUser.userType = User.UserType.FACEBOOK
                                fbSegueUser.fbToken = accessToken.userId
                                self.segueUser = fbSegueUser
                                self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                            }
                        }, failure: { (error) in
                            SVProgressHUD.dismiss()
                            Utilities.showDefaultNetworkError()
                        })
                    }
            }
        }
        
    }
    
    @IBAction func didTapLoginBtn(_ sender: UIButton) {
        let email = emailET.text
        let password = passwET.text
        
        if(!(email?.isEmpty)! && !(password?.isEmpty)!) {
            
            SVProgressHUD.show(with: .gradient)
            
            NetworkManager.sharedInstance.loginWithClassic(email!, password: password!, success: { (user) in
                
                SVProgressHUD.dismiss()
                if((user) != nil) {
                    UserPersistence().persistUser(user!)
                    
                    if let token = FIRInstanceID.instanceID().token() {
                        self.sendFCMTokenToServer(token: token)
                    } else {
                        //Go Home
                        self.performSegue(withIdentifier: "HomeSegue", sender: nil)
                    }
                    
                } else {
                    //Needs registration
                    self.segueUser = User()
                    self.segueUser?.userType = User.UserType.CLASSIC
                    self.segueUser?.email = email
                    self.segueUser?.password = password
                    self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }
                
            }, failure: { (error) in
                
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
                
            })
        } else {
            Utilities.showAlertController("Sorry", message: "Please insert a valid email and password!")
        }
    }
    
    func sendFCMTokenToServer(token:String) {
        if let user = UserPersistence().loadUser() {
            if(!(user.id?.isEmpty)!) {
                NetworkManager.sharedInstance.updateFCMToken(userId: user.id!, token: token, success: { (user) in
                    user?.fcmToken = FIRInstanceID.instanceID().token()
                    UserPersistence().persistUser(user!)
                    //Go Home
                    self.performSegue(withIdentifier: "HomeSegue", sender: nil)
                }, failure: { (error) in
                    
                })
            }
        }
    }
    

    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "RegisterSegue") {
            let regController = segue.destination as! RegistrationViewController
            regController.user = self.segueUser
        }
    }
    

}
