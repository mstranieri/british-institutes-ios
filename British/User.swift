//
//  User.swift
//  British
//
//  Created by MirkoPinna on 07/11/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    fileprivate let idKey = "it.effedodici.British.id"
    fileprivate let emailKey = "it.effedodici.British.email"
    fileprivate let usernameKey = "it.effedodici.British.username"
    fileprivate let passwordKey = "it.effedodici.British.password"
    fileprivate let pictureUrlKey = "it.effedodici.British.picture"
    fileprivate let createdKey = "it.effedodici.British.created"
    fileprivate let pointsKey = "it.effedodici.British.points"
    fileprivate let userTypeKey = "it.effedodici.British.usertype"
    fileprivate let fcmTokenKey = "it.effedodici.British.fcmtoken"
    
    enum UserType : String {
        case CLASSIC, FACEBOOK, UNDEFINED
    }
    
    var id:String?
    var email: String?
    var username: String?
    var password: String?
    var pictureUrl: String?
    var created:String?
    var points:Int?
    var userType:UserType?
    var fcmToken:String?
    
    override init() {
    }
    
    required init?(map: [String: Any]) {
        if let id = map["_id"] as! String? {
            self.id = id
        }
        //self.id = map["_id"] as! String?
        if let email = map["email"] as? String {
            self.email = email
        }
        //self.email = map["email"] as! String?
        if let username = map["username"] as? String{
           self.username = username
        }
        //self.username = map["username"] as! String?
        if let pictureUrl = map["picture_url"] as? String {
            self.pictureUrl = pictureUrl
        }
        //self.pictureUrl = map["picture_url"] as! String?
        
        if let created = map["created_at"] as? String {
            self.created = created
        }
        //self.created = map["created_at"] as! String?
        
        if let points = map["points"] as? Int {
            self.points = points
        } else {
            self.points = 0
        }
        
        if let userType = UserType(rawValue: (map["type"] as! String?)!) {
            self.userType = userType
        }
        //self.userType = UserType(rawValue: (map["type"] as! String?)!)
        
        if let fcmToken = map["notification_token"] as? String {
            self.fcmToken = fcmToken
        }
        //self.fcmToken = map["notification_token"] as! String?
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: idKey) as? String
        email = (aDecoder.decodeObject(forKey: emailKey) != nil) ? aDecoder.decodeObject(forKey: emailKey) as! String : ""
        username = (aDecoder.decodeObject(forKey: usernameKey) != nil) ? aDecoder.decodeObject(forKey: usernameKey) as! String : ""
        password = aDecoder.decodeObject(forKey: passwordKey) as? String
        pictureUrl = (aDecoder.decodeObject(forKey: pictureUrlKey) != nil) ? aDecoder.decodeObject(forKey: pictureUrlKey) as? String : ""
        created = aDecoder.decodeObject(forKey: createdKey) as? String
        points = aDecoder.decodeObject(forKey: pointsKey) as? Int
        userType = UserType(rawValue: (aDecoder.decodeObject(forKey: userTypeKey) as? String)!)
        fcmToken = aDecoder.decodeObject(forKey: fcmTokenKey) as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: idKey)
        aCoder.encode(email, forKey: emailKey)
        aCoder.encode((username != nil) ? username! : "", forKey: usernameKey)
        aCoder.encode(password, forKey: passwordKey)
        aCoder.encode((pictureUrl != nil) ? pictureUrl! : "", forKey: pictureUrlKey)
        aCoder.encode(created, forKey: createdKey)
        aCoder.encode(points, forKey:pointsKey)
        aCoder.encode(userType?.rawValue, forKey:userTypeKey)
        aCoder.encode(fcmToken, forKey:fcmTokenKey)
    }


}
