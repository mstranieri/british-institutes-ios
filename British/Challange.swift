//
//  Challange.swift
//  British
//
//  Created by MirkoPinna on 14/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class Challange: NSObject {
    
    var id:String?
    var starter:User?
    var challanger:User?
    var challangerAccepted:Bool?
    var nextMove:String?
    var createdAt:String?
    var starterState:GameState?
    var challangerState:GameState?
    var terminated:Bool?
    
    required init?(map: [String: Any]) {
        
        self.id = map["_id"] as! String?
        self.starter = User(map:map["starter"] as! [String:AnyObject])
        self.challanger = User(map:map["challanger"] as! [String:AnyObject])
        self.challangerAccepted = map["challanger_accepted"] as! Bool?
        self.nextMove = map["next_move"] as! String?
        self.createdAt = map["created_at"] as! String?
        self.starterState = GameState(map: map["starter_state"] as! [String:AnyObject])
        self.challangerState = GameState(map: map["challanger_state"] as! [String:AnyObject])
        self.terminated = map["terminated"] as! Bool?
        
    }
    

}
