//
//  ChallangesViewController.swift
//  British
//
//  Created by MirkoPinna on 20/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage

class ChallangesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var challangesTableView: UITableView!
    
    var challanges:[Challange] = [Challange]()
    var selectedChallange:Challange?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register for local notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.FCMNotificationKey, object: nil)
        UserDefaults.standard.set(0, forKey: Constants.CHALLENGE_NOTIFICATIONS)
        refreshData()

    }
    
    func refreshData() {
        SVProgressHUD.show()
        if let user = UserPersistence().loadUser() {
            NetworkManager.sharedInstance.userChallanges(userId: user.id!, success: { (challanges) in
                SVProgressHUD.dismiss()
                self.challanges = challanges!
                self.challangesTableView.reloadData()
            }, failure: { (error) in
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK - DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challanges.count
    }
    
    //MARK - TableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let challange = challanges[indexPath.row]
        self.selectedChallange = challange
        self.performSegue(withIdentifier: "ChallangeCatSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChallangeCell", for: indexPath) as! ChallangeTableViewCell
        let challange = challanges[indexPath.row]
        var opponent:User
        if(challange.challanger?.id == UserPersistence().loadUser()?.id) {
            opponent = challange.starter!
        } else {
            opponent = challange.challanger!
        }
        
        
        cell.challangeTitle.text = opponent.username
        if(opponent.id == challange.nextMove) {
            cell.challangeDescription.text = "Opponent turn"
        } else {
            cell.challangeDescription.text = "Your turn"
        }
        
        if let pictureUrl = opponent.pictureUrl as String! {
            Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                if let image = response.result.value {
                    cell.challangePicture.image = image
                }
            })
        } else {
            cell.challangePicture.image = Image(named: "Picture")
        }
        
        return cell
        
    }
    
    func notificationReceived(_ notification: Notification) {
        
        guard let challengeId = notification.userInfo?[Constants.CHALLANGE_ID] as? String else { return }
        
        let notifications = UserDefaults.standard.integer(forKey: Constants.CHALLENGE_NOTIFICATIONS)
        if(notifications > 0) {
            UserDefaults.standard.set(notifications-1, forKey: Constants.CHALLENGE_NOTIFICATIONS)
        }
        
        refreshData()
        
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ChallangeCatSegue") {
            let controller = segue.destination as! CategoriesViewController
            controller.challange = selectedChallange
        }
    }

}
