//
//  GameState.swift
//  British
//
//  Created by MirkoPinna on 14/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class GameState: NSObject {
    
    var answers:Int?
    var points:Int?
    var selectedCategories:[String]?
    
    required init?(map: [String: Any]) {
        self.answers = map["answers"] as! Int?
        self.points = map["points"] as! Int?
        self.selectedCategories = map["selected_categories"] as! [String]?
    }

}
