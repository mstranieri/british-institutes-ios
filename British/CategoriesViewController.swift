//
//  CategoriesViewController.swift
//  British
//
//  Created by MirkoPinna on 15/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD

class CategoriesViewController: BaseViewController {
    
    @IBOutlet weak var scienceBtn: UIButton!
    @IBOutlet weak var sportBtn: UIButton!
    @IBOutlet weak var cultureBtn: UIButton!
    @IBOutlet weak var gossipBtn: UIButton!
    @IBOutlet weak var tvCinemaBtn: UIButton!
    @IBOutlet weak var foodBtn: UIButton!
    @IBOutlet weak var storyBtn: UIButton!
    @IBOutlet weak var musicBtn: UIButton!
    @IBOutlet weak var geographyBtn: UIButton!
    @IBOutlet weak var turnLbl: UILabel!
    @IBOutlet weak var opponentSpinner: UIActivityIndicatorView!
    
    var challange:Challange?
    var question:Question?
    var didGetBackFromQuestion:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register for local notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.FCMNotificationKey, object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        setUp()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func actionWhenBackPressed() {
        if(didGetBackFromQuestion) {
            //User can't go back. Go to Home
            self.performSegue(withIdentifier: "CatBackHome", sender: nil)
        } else {
            super.actionWhenBackPressed()
        }
    }
    
    func setUp() {
        
        if let me = UserPersistence().loadUser() {
            if(me.id == challange?.nextMove) {
                //My turn
                opponentSpinner.isHidden = true
                turnLbl.text = "It's your turn!"
                scienceBtn.isEnabled = true
                sportBtn.isEnabled = true
                cultureBtn.isEnabled = true
                gossipBtn.isEnabled = true
                tvCinemaBtn.isEnabled = true
                foodBtn.isEnabled = true
                storyBtn.isEnabled = true
                musicBtn.isEnabled = true
                geographyBtn.isEnabled = true
            } else {
                opponentSpinner.isHidden = false
                turnLbl.text = "It's your opponent turn!"
                scienceBtn.isEnabled = false
                sportBtn.isEnabled = false
                cultureBtn.isEnabled = false
                gossipBtn.isEnabled = false
                tvCinemaBtn.isEnabled = false
                foodBtn.isEnabled = false
                storyBtn.isEnabled = false
                musicBtn.isEnabled = false
                geographyBtn.isEnabled = false
            }
        }
        
        setSelectedCategories()
    }
    
    //MARK - Actions
    
    @IBAction func backToCategories(_ sender: UIStoryboardSegue) {
        
        if let source = sender.source as? LooseViewController {
            self.challange = source.challange
            didGetBackFromQuestion = true
            setUp()
        }
        
        if let source = sender.source as? WinViewController {
            self.challange = source.challange
            didGetBackFromQuestion = true
            setUp()
        }
        
    }
    
    @IBAction func didTapScience(_ sender:UIButton) {
        goToNextSection(category: Constants.SCIENCE)
    }
    
    @IBAction func didTapSport(_ sender:UIButton) {
        goToNextSection(category: Constants.SPORT)
    }
    
    @IBAction func didTapCulture(_ sender:UIButton) {
        goToNextSection(category: Constants.CULTURE)
    }
    
    @IBAction func didTapGossip(_ sender:UIButton) {
        goToNextSection(category: Constants.GOSSIP)
    }
    
    @IBAction func didTapTVCinema(_ sender:UIButton) {
        goToNextSection(category: Constants.TV_CINEMA)
    }
    
    @IBAction func didTapFood(_ sender:UIButton) {
        goToNextSection(category: Constants.FOOD)
    }
    
    @IBAction func didTapStory(_ sender:UIButton) {
        goToNextSection(category: Constants.HISTORY)
    }
    
    @IBAction func didTapMusic(_ sender:UIButton) {
        goToNextSection(category: Constants.MUSIC)
    }
    
    @IBAction func didTapGeography(_ sender:UIButton) {
        goToNextSection(category: Constants.GEOGRAPHY)
    }
    
    func setSelectedCategories() {
        
        var categories:[String]?
        
        if let me = UserPersistence().loadUser() {
            if(challange?.starter?.id == me.id) {
                categories = (challange?.starterState?.selectedCategories)!
            } else {
                categories = (challange?.challangerState?.selectedCategories)!
            }
        }
        
        if let selectedCat = categories {
            for category in selectedCat {
                if(category == Constants.SCIENCE) {
                    scienceBtn.isEnabled = false
                    scienceBtn.setImage(UIImage(named: "ScienceSelected"), for: UIControlState.disabled)
                }else if(category == Constants.HISTORY) {
                    storyBtn.isEnabled = false
                    storyBtn.setImage(UIImage(named: "StorySelected"), for: UIControlState.disabled)
                }else if(category == Constants.FOOD) {
                    foodBtn.isEnabled = false
                    foodBtn.setImage(UIImage(named: "FoodSelected"), for: UIControlState.disabled)
                }else if(category == Constants.TV_CINEMA) {
                    tvCinemaBtn.isEnabled = false
                    tvCinemaBtn.setImage(UIImage(named: "CinemaSelected"), for: UIControlState.disabled)
                }else if(category == Constants.GOSSIP) {
                    gossipBtn.isEnabled = false
                    gossipBtn.setImage(UIImage(named: "GossipSelected"), for: UIControlState.disabled)
                }else if(category == Constants.MUSIC) {
                    musicBtn.isEnabled = false
                    musicBtn.setImage(UIImage(named: "MusicSelected"), for: UIControlState.disabled)
                }else if(category == Constants.SPORT) {
                    sportBtn.isEnabled = false
                    sportBtn.setImage(UIImage(named: "SportSelected"), for: UIControlState.disabled)
                }else if(category == Constants.GEOGRAPHY) {
                    geographyBtn.isEnabled = false
                    geographyBtn.setImage(UIImage(named: "GeographySelected"), for: UIControlState.disabled)
                }else if(category == Constants.CULTURE) {
                    cultureBtn.isEnabled = false
                    cultureBtn.setImage(UIImage(named: "CultureSelected"), for: UIControlState.disabled)
                }
            }
        }
        
    }
    
    func goToNextSection(category:String) {
        SVProgressHUD.show()
        if let user = UserPersistence().loadUser() {
            NetworkManager.sharedInstance.selectCategory(userId:user.id!, challangeId: (challange?.id)!, category: category, success: {
                (challange, question) in
                self.challange = challange
                self.question = question
                self.performSegue(withIdentifier: "QuestionSegue", sender: nil)
                SVProgressHUD.dismiss()
            }, failure: {
                (error) in
                SVProgressHUD.dismiss()
            })
        }
    }
    
    func notificationReceived(_ notification: Notification) {
                
        guard let challengeId = notification.userInfo?[Constants.CHALLANGE_ID] as? String else { return }
        if(challengeId == challange?.id) {
            let notifications = UserDefaults.standard.integer(forKey: Constants.CHALLENGE_NOTIFICATIONS)
            if(notifications > 0) {
                UserDefaults.standard.set(notifications-1, forKey: Constants.CHALLENGE_NOTIFICATIONS)
            }
            
            SVProgressHUD.show()
            NetworkManager.sharedInstance.getChallange(challengeId: challengeId, success: { (challange) in
                
                SVProgressHUD.dismiss()
                self.challange = challange
                if(challange?.terminated == true) {
                    //Go to final screen
                    self.performSegue(withIdentifier: "CatToFinalSegue", sender: nil)
                } else {
                    self.setUp()
                    self.setSelectedCategories()
                }
                
            }, failure: { (error) in
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
            })
        }
    
    }
    

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "QuestionSegue") {
            let controller = segue.destination as! QuestionViewController
            controller.challange = self.challange
            controller.question = self.question
        }
        
        if(segue.identifier == "CatToFinalSegue") {
            let controller = segue.destination as! FinalViewController
            controller.challange = self.challange
        }
    }
    

}
