//
//  LooseViewController.swift
//  British
//
//  Created by MirkoPinna on 19/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class LooseViewController: BaseViewController {
    
    var challange:Challange?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapContinue(_ sender:UIButton) {
        self.performSegue(withIdentifier: "LooseBackHome", sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
