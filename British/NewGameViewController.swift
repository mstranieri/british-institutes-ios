//
//  NewGameViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewGameViewController: BaseViewController {
    
    var randomOpponent:User?

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapRandomOpponent(_ sender:UIButton) {
        if let user = UserPersistence().loadUser() {
            SVProgressHUD.show()
            NetworkManager.sharedInstance.randomOpponent(user.id!, success: { (user) in
                if(user != nil) {
                    self.randomOpponent = user
                    self.performSegue(withIdentifier: "RandomOpponentSegue", sender: nil)
                }
                SVProgressHUD.dismiss()
            }, failure: { (error) in
                Utilities.showDefaultNetworkError()
                SVProgressHUD.dismiss()
            })
        }
    }
    
    

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "RandomOpponentSegue") {
            let controller = segue.destination as! OpponentDetailViewController
            controller.opponent = randomOpponent
        }
    }

}
