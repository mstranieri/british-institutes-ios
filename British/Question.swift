//
//  Question.swift
//  British
//
//  Created by MirkoPinna on 14/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class Question: NSObject {
    
    var question:String?
    var answers:[String]?
    var correct:Int?
    var points:Int?
    
    required init?(map: [String: Any]) {
        self.question = map["question"] as! String?
        self.answers = map["answers"] as! [String]?
        self.correct = map["correct"] as! Int?
        self.points = map["points"] as! Int?
    }

}
