//
//  RegistrationViewController.swift
//  British
//
//  Created by MirkoPinna on 13/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import FacebookCore
import AlamofireImage
import Alamofire
import SVProgressHUD
import Firebase

class RegistrationViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var registerPicture: UIImageView!
    @IBOutlet weak var registerUsername: UITextField!
    
    var user:User?
    var fbPictureUrl:String?
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        
        if(user?.userType == User.UserType.FACEBOOK) {
            let connection = GraphRequestConnection()
            connection.add(GraphRequest(graphPath: "/me")) { httpResponse, result in
                switch result {
                case .success(let response):
                    let fbID = response.dictionaryValue?["id"] as! String
                    if let name = response.dictionaryValue?["name"] as! String? {
                        self.registerUsername.text = name.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                    }
                    self.fbPictureUrl = "http://graph.facebook.com/" + fbID + "/picture?type=large"
                    self.user?.pictureUrl = self.fbPictureUrl
                    Alamofire.request(self.fbPictureUrl!).responseImage(completionHandler: { (response) in
                        if let image = response.result.value {
                            self.registerPicture.image = image
                        }
                    })
                    
                    print("Graph Request Succeeded: \(response)")
                case .failed(let error):
                    print("Graph Request Failed: \(error)")
                }
            }
            connection.start()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapProfilePicture(_ sender:UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func didTapContinue(_ sender:UIButton) {
        if(!(registerUsername.text?.isEmpty)!) {
            
            if let username = registerUsername.text as String! {
                self.user?.username = username
            }
            
            //Assign fcm token if exists
            if let fcmToken = FIRInstanceID.instanceID().token() {
                self.user?.fcmToken = fcmToken
            }
            
            
            if(user?.userType == User.UserType.FACEBOOK) {
                SVProgressHUD.show()
                NetworkManager.sharedInstance.registerFB(user: user as! FBUser, success: { (user) in
                    SVProgressHUD.dismiss()
                    if(user != nil) {
                        UserPersistence().persistFBUser(user!)
                        self.performSegue(withIdentifier: "RegHomeSegue", sender: nil)
                    }
                }, failure: { (error) in
                    SVProgressHUD.dismiss()
                })
            } else {
                SVProgressHUD.show()
                NetworkManager.sharedInstance.register(user: user!, success: { (user) in
                    SVProgressHUD.dismiss()
                    if(user != nil) {
                        UserPersistence().persistUser(user!)
                        self.performSegue(withIdentifier: "RegHomeSegue", sender: nil)
                    }
                }, failure: { (error) in
                    SVProgressHUD.dismiss()
                })
            }
        }
    }

        
    
    // MARK: - Image
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage? {
            SVProgressHUD.show()
            NetworkManager.sharedInstance.uploadPicture(image: chosenImage, userId: "user_" + String(Int(arc4random())), success: { (pictureUrl) in
                
                SVProgressHUD.dismiss()
                if let picture = pictureUrl as String! {
                    self.user?.pictureUrl = picture
                }
                
                self.registerPicture.contentMode = .scaleAspectFit
                self.registerPicture.image = chosenImage
                
            }, failure: { (error) in
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
            })
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
