//
//  AppDelegate.swift
//  British
//
//  Created by MirkoPinna on 22/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import FacebookCore
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Fabric
import Crashlytics
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Fabric.with([Crashlytics.self])
        
        IQKeyboardManager.sharedManager().enable = true
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FIRApp.configure()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        //If the app is started by a notification show the challanges section
        if let notification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            if let navigationController = window?.rootViewController as? UINavigationController {
                if let homecontroller = navigationController.viewControllers.first as? HomeViewController {
                    NetworkManager.sharedInstance.getChallange(challengeId: notification[Constants.CHALLANGE_ID] as! String, success: { (challenge) in
                        homecontroller.notificationChallange = challenge
                        homecontroller.performSegue(withIdentifier: "HomeToCategoriesSegue", sender: nil)
                    }, failure: { (error) in
                        Utilities.showDefaultNetworkError()
                    })
                }
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return SDKApplicationDelegate.shared.application(application,
                                                         open: url,
                                                         sourceApplication: sourceApplication,
                                                         annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return SDKApplicationDelegate.shared.application(application, open: url, options: options)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        FIRMessaging.messaging().disconnect()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: Notifications
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        manageNotification(userInfo: userInfo)
    }
    
    func manageNotification(userInfo: [AnyHashable : Any]) {
        let status = userInfo["status"] as! String
        let challengeId = userInfo["challange_id"] as! String
        
        if(status == Constants.CHALLANGE_STARTED) {
            let notifications = UserDefaults.standard.integer(forKey: Constants.CHALLENGE_NOTIFICATIONS)
            UserDefaults.standard.set(notifications+1, forKey: Constants.CHALLENGE_NOTIFICATIONS)
            
            //Send local notification
            let userInfo = [ Constants.STATUS : status, Constants.CHALLANGE_ID : challengeId]
            NotificationCenter.default.post(name: .FCMNotificationKey, object: nil, userInfo: userInfo)
            
        } else if(status == Constants.OPPONENT_RESPONDED) {
            let notifications = UserDefaults.standard.integer(forKey: Constants.CHALLENGE_NOTIFICATIONS)
            UserDefaults.standard.set(notifications+1, forKey: Constants.CHALLENGE_NOTIFICATIONS)
            
            //Send local notification
            let userInfo = [ Constants.STATUS : status, Constants.CHALLANGE_ID : challengeId]
            NotificationCenter.default.post(name: .FCMNotificationKey, object: nil, userInfo: userInfo)
        }
    }
    
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
                //Send token to server if user is logged
                if let user = UserPersistence().loadUser() {
                    if(!(user.id?.isEmpty)!) {
                        NetworkManager.sharedInstance.updateFCMToken(userId: user.id!, token: FIRInstanceID.instanceID().token()!, success: { (user) in
                            user?.fcmToken = FIRInstanceID.instanceID().token()
                            UserPersistence().persistUser(user!)
                        }, failure: { (error) in
                            
                        })
                    }
                }
                
            }
        }
    }
}

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        manageNotification(userInfo: remoteMessage.appData)
}
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        manageNotification(userInfo: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        manageNotification(userInfo: userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        manageNotification(userInfo: userInfo)
        
        completionHandler()
    }
}

