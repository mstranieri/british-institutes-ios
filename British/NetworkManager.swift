//
//  NetworkManager.swift
//  British
//
//  Created by MirkoPinna on 07/11/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import Alamofire


class NetworkManager: NSObject {
    
    fileprivate let manager: SessionManager!
    
    static let sharedInstance = NetworkManager()
    
    //var urlBase : String?
    
    override init() {
        
        let configuration = URLSessionConfiguration.default
        
        //configuration.HTTPAdditionalHeaders = defaultHeaders
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 300
        
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func loginWithClassic(_ username: String, password: String, success: ((User?) -> Void)?, failure: ((NSError) -> Void)?) {
        let url = Constants.BaseUrl + Constants.LoginUrl
        
        manager.request(url, method: .post, parameters: ["email" : username, "password" : password], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let logged = json["logged"] as! Bool? else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            if(logged == true) {
                guard let user = User(map: json["user"] as! [String : Any]) else {
                    if let failureBlock = failure {
                        DispatchQueue.main.async(execute: { () -> Void in
                            failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                        })
                    }
                    return
                }
                success!(user)
            } else {
                success!(nil)
            }
            
        }
        
    }
    
    func loginWithFB(token:String, success: ((FBUser?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.FBLoginUrl
        
        manager.request(url, method: .post, parameters: ["token" : token], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            
            guard let logged = json["logged"] as! Bool? else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            if(logged == true) {
                guard let user = FBUser(map: json["user"] as! [String : Any]) else {
                    if let failureBlock = failure {
                        DispatchQueue.main.async(execute: { () -> Void in
                            failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                        })
                    }
                    return
                }
                success!(user)
            } else {
                success!(nil)
            }
            
        }
        
    }
    
    func register(user:User, success: ((User?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.RegisterUrl
        
        var picture_url = user.pictureUrl ?? ""
        
        manager.request(url, method: .post, parameters: ["email":user.email!, "username":user.username!, "password":user.password!, "picture":picture_url, "fcm_token":user.fcmToken], encoding: URLEncoding.default, headers:[:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }

            guard let user = User(map: json["user"] as! [String:Any]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(user)
            
        }
        
    }
    
    func registerFB(user:FBUser, success: ((FBUser?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.RegisterFBUrl
        
        
        manager.request(url, method: .post, parameters: ["username":user.username!, "fb_token":user.fbToken!, "picture_url":user.pictureUrl!, "fcm_token":user.fcmToken ?? ""], encoding: URLEncoding.default, headers:[:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let user = FBUser(map: json["user"] as! [String:Any]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(user)
            
        }
        
    }

    
    func uploadPicture(image:UIImage, userId:String, success: ((String?) -> Void)?, failure: ((NSError?) -> Void)?){
        
        let url = Constants.BaseUrl + Constants.PictureUpload
        
        manager.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(image, 0.3) {
                multipartFormData.append(imageData, withName: "file", fileName: "picture.jpg", mimeType: "image/jpeg")
            }
            
            multipartFormData.append(userId.data(using: String.Encoding.utf8)!, withName: "user_id")
            
        }, to: url, encodingCompletion: {
            encodingResult in
            
            switch(encodingResult) {
            case .success(let upload, _, _):
                print(upload)
                upload.responseString(completionHandler: { (response) in
                    switch(response.result) {
                    case .success(let value):
                        let failureInvocation = { () in
                            if let invocation = failure {
                                invocation(nil)
                            }
                        }
                        
                        
                        guard let jsonData = value.data(using: String.Encoding.utf8) else {
                            failureInvocation()
                            
                            return
                        }
                        
                        guard let json = try? JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String:AnyObject] else {
                            failureInvocation()
                            
                            return
                        }
                        
                        let responseError = json?["error"] as? String
                        if(responseError != nil && responseError != ""){
                            failure!(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey : responseError!]))
                        }
                        
                        
                        guard let pictureUrl = json?["picture_url"] as? String else {
                            failureInvocation()
                            
                            return
                        }

                        success!(pictureUrl)
                        
                    case .failure(let error):
                        print(error)
                        
                        if let invocation = failure {
                            invocation(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                        }
                    }
                })
                
            case .failure(let encodingError):
                print(encodingError)
            }
        })
        
    }
    
    func randomOpponent(_ userId:String, success: ((User?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.RandomOpponentUrl
        
        manager.request(url, method: .post, parameters: ["user_id":userId], encoding: URLEncoding.default, headers: [:]).responseJSON {
            (response) in
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let user = User(map: json["user"] as! [String:Any]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(user)

        }
    }
    
    func getUsers(myId: String, success: ((_ users:[User]?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.UsersUrl
        
        manager.request(url, method: .post, parameters: ["user_id":myId], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            var users:[User] = []
            if let usersJson = json["users"] as? [[String:AnyObject]] {
                for element in usersJson {
                    if let user = User(map: element) {
                        users.append(user)
                    }
                }
            }
            
            success!(users)

        }
        
    }
    
    func startChallange(starterId:String, challangerId:String, success: ((Challange?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.StartChallangeUrl
        
        manager.request(url, method: .post, parameters: ["starter_id":starterId, "challanger_id":challangerId], encoding: URLEncoding.default, headers: [:]).responseJSON {
            (response) in
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let challange = Challange(map: json["challange"] as! [String:AnyObject]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(challange)

        }
        
    }
    
    func selectCategory(userId:String, challangeId:String, category:String, success: ((Challange?, Question?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.SelectedCategoryUrl
        
        manager.request(url, method: .post, parameters: ["user_id":userId, "challange_id":challangeId, "category":category], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let challange = Challange(map: json["challange"] as! [String:AnyObject]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let question = Question(map: json["question"] as! [String:AnyObject]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(challange, question)

        }
    }
    
    func answer(right:String, challangeId:String, userId:String, points:Int, success: ((Challange?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.AnswerUrl
        
        manager.request(url, method: .post, parameters: ["answer_right":right, "challange_id":challangeId, "user_id":userId, "points":points], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let challange = Challange(map: json["challange"] as! [String:AnyObject]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }

            success!(challange)
            
        }
        
    }
    
    func userChallanges(userId:String, success: (([Challange]?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.ChallangesUrl
        
        manager.request(url, method: .post, parameters: ["user_id":userId], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            var challanges:[Challange] = []
            if let challangesJson = json["challanges"] as? [[String:AnyObject]] {
                for element in challangesJson {
                    if let challange = Challange(map: element) {
                        challanges.append(challange)
                    }
                }
            }
            
            success!(challanges)
 
        }
    }
    
    func getUsersByScore(success: ((_ users:[User]?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.UsersByScoreUrl
        
        manager.request(url).responseJSON {
            (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            var users:[User] = []
            if let usersJson = json["users"] as? [[String:AnyObject]] {
                for element in usersJson {
                    if let user = User(map: element) {
                        users.append(user)
                    }
                }
            }
            
            success!(users)
        }
    }
    
    func getChallange(challengeId:String, success: ((Challange?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.ChallangeUrl
        
        manager.request(url, method: .post, parameters: ["challange_id": challengeId], encoding: URLEncoding.default, headers:[:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let challange = Challange(map: json["challange"] as! [String:Any]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(challange)
            
        }
        
    }
    
    func updateFCMToken(userId:String, token:String, success: ((User?) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = Constants.BaseUrl + Constants.FCMTokenUrl
        
        manager.request(url, method: .post, parameters: ["user_id":userId, "token":token], encoding: URLEncoding.default, headers:[:]).responseJSON { (response) in
            
            guard response.result.error == nil else {
                print(response.result.error!)
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            // make sure we got JSON and it's a dictionary
            guard let json = response.result.value as? [String: AnyObject] else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            guard let user = User(map: json["user"] as! [String:Any]) else {
                if let failureBlock = failure {
                    DispatchQueue.main.async(execute: { () -> Void in
                        failureBlock(NSError(domain: "it.effedodici.British", code: 666, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString(Localisations.NetworkManagerDefaultError, comment: "")]))
                    })
                }
                return
            }
            
            success!(user)
            
        }
        
    }

}
