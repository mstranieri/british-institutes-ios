//
//  FinalViewController.swift
//  British
//
//  Created by MirkoPinna on 15/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class FinalViewController: BaseViewController {

    @IBOutlet weak var finalTopLbl: UILabel!
    @IBOutlet weak var finalImg: UIImageView!
    @IBOutlet weak var finalBottomLbl: UILabel!
    
    var challange:Challange?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let me = UserPersistence().loadUser() {
            if(challange != nil) {
                let amIStarter = (me.id == challange?.starter?.id)
                if(amIStarter) {
                    if((challange?.starterState?.answers)! >= 5) {
                        setUpWinnerScreen(points: (challange?.starterState?.points)!)
                    } else {
                        setUpLoserScreen(points: (challange?.starterState?.points)!)
                    }
                } else {
                    if((challange?.starterState?.answers)! >= 5) {
                        setUpLoserScreen(points: (challange?.starterState?.points)!)
                    } else {
                        setUpWinnerScreen(points: (challange?.starterState?.points)!)
                    }
                }
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapContinue(_ sender:UIButton) {
        self.performSegue(withIdentifier: "FinalUnwindHome", sender: nil)
    }
    
    func setUpLoserScreen(points:Int) {
        finalTopLbl.text = "Oh no, you lose!"
        finalBottomLbl.text = "You got only \(points) Points \n You should study more!"
        finalImg.image = UIImage(named: "FinalLose")
    }
    
    func setUpWinnerScreen(points:Int) {
        finalTopLbl.text = "Congratulations"
        finalBottomLbl.text = "You won! \(points) Points"
        finalImg.image = UIImage(named: "FinalWinCup")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
