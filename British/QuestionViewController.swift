//
//  QuestionViewController.swift
//  British
//
//  Created by MirkoPinna on 15/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD
import MLVerticalProgressView
import Foundation

class QuestionViewController: BaseViewController {
    
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var answer1Btn: UIButton!
    @IBOutlet weak var answer2Btn: UIButton!
    @IBOutlet weak var answer3Btn: UIButton!
    @IBOutlet weak var answer4Btn: UIButton!
    @IBOutlet weak var questionProgress: VerticalProgressView!
    @IBOutlet weak var questionTimeLbl: UILabel!
    
    var question:Question?
    var challange:Challange?
    
    var canGoNext:Bool = false
    var didUpdateChallange:Bool?
    var isCorrect:Bool?
    
    var timeoutTimer:Timer?
    var delayTimer:Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(question != nil) {
            questionLbl.text = question?.question
            answer1Btn.setTitle(question?.answers?[0], for: UIControlState.normal)
            answer2Btn.setTitle(question?.answers?[1], for: UIControlState.normal)
            answer3Btn.setTitle(question?.answers?[2], for: UIControlState.normal)
            answer4Btn.setTitle(question?.answers?[3], for: UIControlState.normal)
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if #available(iOS 10.0, *) {
            var count = 30
            timeoutTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
                (timer) in
                
                let progress = Double(count) * 0.033
                
                self.questionProgress.setProgress(progress: Float(progress), animated: false)
                self.questionTimeLbl.text = String(count) + "\""
                
                if(count <= 0) {
                    //Finish
                    timer.invalidate()
                    self.questionProgress.setProgress(progress: 0, animated: false)
                    self.questionTimeLbl.text = "0\""
                    
                    self.checkAnswer(answer: 300, selectedView: nil)

                }
                
                count -= 1
                
            })
        } else {
        }
    }
    
    func checkAnswer(answer:Int, selectedView:UIButton?) {
        
        self.timeoutTimer?.invalidate()
        self.timeoutTimer = nil
        
        isCorrect = (answer == question?.correct)
        let rightImg = UIImage(named: "RightAnswerBtn")
        if(isCorrect)! {
            selectedView?.setBackgroundImage(rightImg, for: UIControlState.normal)
        } else {
            let image = UIImage(named: "WrongAnswerBtn")
            selectedView?.setBackgroundImage(image, for: UIControlState.normal)
            if let correct = question?.correct {
                switch correct {
                case 1:
                    answer1Btn.setBackgroundImage(rightImg, for: UIControlState.normal)
                case 2:
                    answer2Btn.setBackgroundImage(rightImg, for: UIControlState.normal)
                case 3:
                    answer3Btn.setBackgroundImage(rightImg, for: UIControlState.normal)
                case 4:
                    answer4Btn.setBackgroundImage(rightImg, for: UIControlState.normal)
                default: break
                }
            }
        }
        
        
        if #available(iOS 10.0, *) {
            delayTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: {
                (timer) in
                self.canGoNext = true
                if(self.didUpdateChallange)! {
                    //Move to Categories
                    SVProgressHUD.dismiss()
                    /*if(self.isCorrect)! {
                      self.performSegue(withIdentifier: "YouWonSegue", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "YouLooseSegue", sender: nil)
                    }*/
                    self.goToFinalScreen(challange: self.challange!)
                }
            })
        } else {
            canGoNext = true
        }
        
        SVProgressHUD.show()
        let userId = UserPersistence().loadUser()?.id
        NetworkManager.sharedInstance.answer(right: isCorrect! ? "yes" : "no", challangeId: (challange?.id!)!, userId: userId!, points: (self.question?.points)!, success: {(challange) in
            self.didUpdateChallange = true
            self.challange = challange
            
            self.goToFinalScreen(challange: challange!)
            
            /*if(challange?.terminated)! {
                //Move to final screen
                SVProgressHUD.dismiss()
                self.delayTimer?.invalidate()
                self.delayTimer = nil
                self.performSegue(withIdentifier: "FinalScreenSegue", sender: nil)
            } else {
                if(self.canGoNext) {
                    //Move to Categories
                    SVProgressHUD.dismiss()
                    if(challange?.starter?.id == userId) {
                        UserPersistence().persistUser((challange?.starter)!)
                    } else if(challange?.challanger?.id == userId) {
                        UserPersistence().persistUser((challange?.challanger)!)
                    }
                    
                    if(self.isCorrect)! {
                        self.performSegue(withIdentifier: "YouWonSegue", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "YouLooseSegue", sender: nil)
                    }
                    //self.performSegue(withIdentifier: "UnwindCategoriesSegue", sender: nil)
                }
            }*/
        }, failure: { (error) in
            Utilities.showDefaultNetworkError()
        })
        
        
    }
    
    func goToFinalScreen(challange:Challange) {
        if(challange.terminated)! {
            //Move to final screen
            SVProgressHUD.dismiss()
            delayTimer?.invalidate()
            delayTimer = nil
            self.performSegue(withIdentifier: "FinalScreenSegue", sender: nil)
        } else {
            if(self.canGoNext) {
                //Move to Categories
                let userId = UserPersistence().loadUser()?.id
                SVProgressHUD.dismiss()
                if(challange.starter?.id == userId) {
                    UserPersistence().persistUser((challange.starter)!)
                } else if(challange.challanger?.id == userId) {
                    UserPersistence().persistUser((challange.challanger)!)
                }
                
                if(isCorrect)! {
                    self.performSegue(withIdentifier: "YouWonSegue", sender: nil)
                } else {
                    self.performSegue(withIdentifier: "YouLooseSegue", sender: nil)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapAnswer1(_ sender:UIButton) {
        checkAnswer(answer: 1, selectedView: sender)
    }
    
    @IBAction func didTapAnswer2(_ sender:UIButton) {
        checkAnswer(answer: 2, selectedView: sender)
    }
    
    @IBAction func didTapAnswer3(_ sender:UIButton) {
        checkAnswer(answer: 3, selectedView: sender)
    }
    
    @IBAction func didTapAnswer4(_ sender:UIButton) {
        checkAnswer(answer: 4, selectedView: sender)
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "YouWonSegue") {
            let controller = segue.destination as! WinViewController
            controller.challange = self.challange
        }
        
        if(segue.identifier == "YouLooseSegue") {
            let controller = segue.destination as! LooseViewController
            controller.challange = self.challange
        }
        
        if(segue.identifier == "CatToFinalSegue") {
            let controller = segue.destination as! FinalViewController
            controller.challange = self.challange
        }

    }

}
