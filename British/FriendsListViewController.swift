//
//  FriendsListViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD

class FriendsListViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var friendsCollection: UICollectionView!
    
    var users:[User] = [User]()
    var selectedUser:User?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        if let me = UserPersistence().loadUser() {
            NetworkManager.sharedInstance.getUsers(myId: me.id!, success: { (users) in
                SVProgressHUD.dismiss()
                if(users != nil) {
                    self.users = users!
                    self.friendsCollection.reloadData()
                }
            }, failure: {(error) in
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
            })

        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "FriendOpponentSegue") {
            let controller = segue.destination as! OpponentDetailViewController
            controller.opponent = selectedUser
        }
    }
    
    //MARK - DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendCell",for: indexPath) as! FriendCollectionViewCell
        if(users.count >= indexPath.row) {
            let user = users[indexPath.row]
            cell.friendUsername.text = user.username
            if let pictureUrl = user.pictureUrl as String! {
                Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                    if let image = response.result.value {
                        cell.friendPicture.image = image
                    }
                })
            }

        }
        
        
        return cell
    }
    
    
    //MARK - CollectioDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedUser = users[indexPath.row]
        self.performSegue(withIdentifier: "FriendOpponentSegue", sender: nil)
        
    }
    
}
