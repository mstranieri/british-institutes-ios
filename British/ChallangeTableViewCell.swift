//
//  ChallangeTableViewCell.swift
//  British
//
//  Created by MirkoPinna on 20/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class ChallangeTableViewCell: UITableViewCell {

    @IBOutlet weak var challangePicture: UIImageView!
    @IBOutlet weak var challangeTitle: UILabel!
    @IBOutlet weak var challangeDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
