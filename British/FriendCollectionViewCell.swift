//
//  FriendCollectionViewCell.swift
//  British
//
//  Created by MirkoPinna on 20/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class FriendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var friendPicture: UIImageView!
    @IBOutlet weak var friendUsername: UILabel!
    
}
