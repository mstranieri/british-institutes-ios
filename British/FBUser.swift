//
//  FBUser.swift
//  British
//
//  Created by MirkoPinna on 12/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class FBUser: User {
    
    fileprivate let FBTokenKey = "it.effedodici.British.fbtoken"
    
    var fbToken:String?
    
    override init() {
       super.init()
    }
    
    required init?(map: [String: Any]) {
        self.fbToken = map["fbToken"] as! String?
        super.init(map: map)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fbToken = aDecoder.decodeObject(forKey: FBTokenKey) as? String
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(fbToken, forKey: FBTokenKey)
        super.encode(with: aCoder)
    }

}
