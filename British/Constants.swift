//
//  Constants.swift
//  British
//
//  Created by MirkoPinna on 07/11/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

open class Constants: NSObject {
    
    static let LoginTypeClassic = "CLASSIC"
    static let LoginTypeFB = "FACEBOOK"
    
    //static let BaseUrl = "http://localhost:3000"
    static let BaseUrl = "http://api-bi.effedodici.it:3000"
    static let LoginUrl = "/auth/login"
    static let FBLoginUrl = "/auth/fblogin"
    static let PictureUpload = "/profile/picture"
    static let RegisterUrl = "/auth/register"
    static let RegisterFBUrl = "/auth/fbregister"
    static let RandomOpponentUrl = "/randomopponent"
    static let UsersUrl = "/getusers"
    static let StartChallangeUrl = "/startchallange"
    static let SelectedCategoryUrl = "/selectcategory"
    static let AnswerUrl = "/answer"
    static let ChallangesUrl = "/user/challanges"
    static let UsersByScoreUrl = "/getusers/score"
    static let FCMTokenUrl = "/notificationToken"
    static let ChallangeUrl = "/getchallange"
    
    
    static let SCIENCE = "SCIENCE";
    static let HISTORY = "HISTORY";
    static let FOOD = "FOOD";
    static let TV_CINEMA = "TV_CINEMA";
    static let GOSSIP = "GOSSIP";
    static let MUSIC = "MUSIC";
    static let SPORT = "SPORT";
    static let GEOGRAPHY = "GEOGRAPHY";
    static let CULTURE = "CULTURE";
    
    //Notifications
    static let STATUS = "status";
    static let CHALLANGE_ID = "challange_id";
    static let CHALLANGE_STARTED = "CHALLANGE_STARTED";
    static let OPPONENT_RESPONDED = "OPPONENT_RESPONDED";
    static let CHALLENGE_NOTIFICATIONS = "challange_notifications";
    static let NOTIFICATION_ON = "notificarion_on";

}

extension Notification.Name {
    public static let FCMNotificationKey = Notification.Name(rawValue: "fcmnotificationkey")
}
