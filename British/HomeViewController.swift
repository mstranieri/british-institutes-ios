//
//  HomeViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD

class HomeViewController: BaseViewController {

    @IBOutlet weak var pictureImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var notificationLbl: UILabel!
    
    var user:User?
    
    var notificationChallange:Challange?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationLbl.layer.backgroundColor  = UIColor.red.cgColor
        notificationLbl.layer.cornerRadius = notificationLbl.frame.width/2
        notificationLbl.layer.masksToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Register for local notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.FCMNotificationKey, object: nil)
        
        fillUserInfos()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func fillUserInfos() {
        if let myuser = UserPersistence().loadUser() {
            self.user = myuser
            
            if let pictureUrl = user?.pictureUrl as String! {
                Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                    if let image = response.result.value {
                        self.pictureImg.image = image
                    }
                })
            }
            
            if let username = user?.username as String! {
                usernameLbl.text = username
            }
            
            if let points = user?.points as Int! {
                pointsLbl.text = "\(points)  Points"
            }
            
            let notifications = UserDefaults.standard.integer(forKey: Constants.CHALLENGE_NOTIFICATIONS)
            
            if(notifications > 0) {
                notificationLbl.text = String(notifications)
                notificationLbl.isHidden = false
            } else {
                notificationLbl.isHidden = true
            }
        
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapChallanges(_ sender:UIButton) {
        self.performSegue(withIdentifier: "ChallangesSegue", sender: nil)
    }
    
    @IBAction func didTapShare(_ sender:UIButton) {
        
        let activityViewController = UIActivityViewController(activityItems: ["https://www.britishinstitutes.it"], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func unwindBackHome(_ sender:UIStoryboardSegue) {
        
    }
    
    func notificationReceived(_ notification: Notification) {
        
        fillUserInfos()
        
        if let status = notification.userInfo?[Constants.STATUS] as? String {
            
            let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                (alertAction) in
                //Did tap Yes
                if let challangeID = notification.userInfo?[Constants.CHALLANGE_ID] as? String {
                    SVProgressHUD.show()
                    NetworkManager.sharedInstance.getChallange(challengeId: challangeID, success: { (challenge) in
                        SVProgressHUD.dismiss()
                        self.notificationChallange = challenge
                        self.performSegue(withIdentifier: "HomeToCategoriesSegue", sender: nil)
                    }, failure: { (error) in
                        SVProgressHUD.dismiss()
                        Utilities.showDefaultNetworkError()
                    })
                }
                
            })
            
            let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: {
                (alertAction) in
                //Did tap No
            })
            
            if(status == Constants.CHALLANGE_STARTED) {
                Utilities.showAlertController("New Challenge", message: "Somebody wants to challenge you, would you like to play?", actions: [yesAction, noAction], style: UIAlertControllerStyle.alert, presentingViewController: self)
            } else if(status == Constants.OPPONENT_RESPONDED) {
                Utilities.showAlertController("Challenge Update", message: "It's your turn, would you like to play?", actions: [yesAction, noAction], style: UIAlertControllerStyle.alert, presentingViewController: self)
            }
            
        }
        
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "HomeToCategoriesSegue") {
            let controller = segue.destination as! CategoriesViewController
            controller.challange = notificationChallange
        }
    }
    

}
