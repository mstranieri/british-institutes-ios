//
//  ScoreViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import Alamofire

class ScoreViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var scoreTableView: UITableView!
    
    var users:[User] = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        NetworkManager.sharedInstance.getUsersByScore(success: { (users) in
            SVProgressHUD.dismiss()
            self.users = users!
            self.scoreTableView.reloadData()
        }, failure: {(error) in
            SVProgressHUD.dismiss()
            Utilities.showDefaultNetworkError()
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK - DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    //MARK - TableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreCell", for: indexPath) as! ScroreTableViewCell
        let user = users[indexPath.row]
        cell.scorePositionLbl.text = String(indexPath.row+1) + "°"
        if(indexPath.row == 0) {
            //GOLD
            cell.scorePositionLbl.textColor = UIColor(red:0.98, green:0.69, blue:0.24, alpha:1.0)
        } else if(indexPath.row == 1) {
            //Grey
            cell.scorePositionLbl.textColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0)
        } else if(indexPath.row == 2) {
            //Amaranto
            cell.scorePositionLbl.textColor = UIColor(red:0.60, green:0.15, blue:0.07, alpha:1.0)
        } else {
            //Black
            cell.scorePositionLbl.textColor = UIColor.black
        }
        cell.scoreTitleLbl.text = user.username
        if let points = user.points {
            cell.scorePointsLbl.text = String(describing: points) + " Points"
        }
        if let pictureUrl = user.pictureUrl as String! {
            Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                if let image = response.result.value {
                    cell.scoreImg.image = image
                }
            })
        }
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
