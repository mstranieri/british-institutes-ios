//
//  OpponentDetailViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD

class OpponentDetailViewController: BaseViewController {
    
    @IBOutlet weak var opponentNameLbl: UILabel!
    @IBOutlet weak var opponentPicture: UIImageView!
    @IBOutlet weak var totalPointsLbl: UILabel!
    @IBOutlet weak var bestMatchLbl: UILabel!
    @IBOutlet weak var bestQuestionLbl: UILabel!
    @IBOutlet weak var winsLbl: UILabel!
    @IBOutlet weak var loseLbl: UILabel!
    @IBOutlet weak var drawLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    var opponent:User?
    var challange:Challange?
    
    var hasOpenChallenge:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(opponent != nil) {
            opponentNameLbl.text = opponent?.username
            
            if let pictureUrl = opponent?.pictureUrl {
                Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                    if let image = response.result.value {
                        self.opponentPicture.image = image
                    }
                })
            }
            
            if let points = opponent?.points {
                totalPointsLbl.text = String(points) + " Points"
            }
            
            SVProgressHUD.show()
            NetworkManager.sharedInstance.userChallanges(userId: (UserPersistence().loadUser()?.id)!, success: { (challanges) in
                SVProgressHUD.dismiss()
                for challange in challanges! {
                    if(challange.challanger?.id == self.opponent?.id || challange.starter?.id == self.opponent?.id) {
                        self.hasOpenChallenge = true
                    }
                }
            }, failure: { (error) in
                SVProgressHUD.dismiss()
                Utilities.showDefaultNetworkError()
            })
            
        }
    }
    
    @IBAction func didTapPlay(_ sender:UIButton) {
        
        if(!hasOpenChallenge) {
            let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                (alertAction) in
                //Did tap Yes
                if let myId = UserPersistence().loadUser()?.id {
                    SVProgressHUD.show()
                    NetworkManager.sharedInstance.startChallange(starterId: myId, challangerId: (self.opponent?.id)!, success: { (challange) in
                        SVProgressHUD.dismiss()
                        if(challange != nil) {
                            self.challange = challange
                            self.performSegue(withIdentifier: "CategoriesSegue", sender: nil)
                        }
                    }, failure: { (error) in
                        Utilities.showDefaultNetworkError()
                        SVProgressHUD.dismiss()
                    })
                }
            })
            
            let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: {
                (alertAction) in
                //Did tap No
            })
            
            
            Utilities.showAlertController("", message: "Do you want to challange " + (opponent?.username)!, actions: [yesAction, noAction], style: UIAlertControllerStyle.alert, presentingViewController: self)
        } else {
            Utilities.showAlertController("", message: "You already started a challenge with this user. You can continue it from the challanges list.", actions: [], style: UIAlertControllerStyle.alert, presentingViewController: self)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CategoriesSegue") {
            let controller = segue.destination as! CategoriesViewController
            controller.challange = self.challange
        }
    }
    

}
