//
//  UserPersistence.swift
//  British
//
//  Created by MirkoPinna on 12/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class UserPersistence {
    
    fileprivate let userKey = "it.effedodici.British.user"
    
    func persistUser(_ user: User) {
        
        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(userData, forKey: userKey)
        UserDefaults.standard.synchronize()
        
    }
    
    func persistFBUser(_ user: FBUser) {
        
        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(userData, forKey: userKey)
        UserDefaults.standard.synchronize()
        
    }
    
    func loadUser() -> User? {
        let data = UserDefaults.standard.object(forKey: userKey)
        
        if let userData = data as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User {
                return user
            }
        }
        
        return nil
    }
    
    func loadFBUser() -> FBUser? {
        let data = UserDefaults.standard.object(forKey: userKey)
        
        if let userData = data as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? FBUser {
                return user
            }
        }
        return nil
    }
    
    func deleteUser() -> Bool {
        if let _ = loadUser() {
            UserDefaults.standard.removeObject(forKey: userKey)
            UserDefaults.standard.synchronize()
            return false
        } else {
            return false
        }
    }


}
