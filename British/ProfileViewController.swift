//
//  ProfileViewController.swift
//  British
//
//  Created by MirkoPinna on 24/08/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import AlamofireImage

class ProfileViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        
        if let user = UserPersistence().loadUser() {
            profileNameLbl.text = user.username
            if let points = user.points {
                totalPointsLbl.text = String(points) + " Points"
            }
            
            if let pictureUrl = user.pictureUrl as String! {
                Alamofire.request(pictureUrl).responseImage(completionHandler: { (response) in
                    if let image = response.result.value {
                        self.profilePicture.image = image
                    }
                })
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapPicture(_ sender:UIButton) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    //MARK - ImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage? {
            SVProgressHUD.show()
            if let user = UserPersistence().loadUser() {
                NetworkManager.sharedInstance.uploadPicture(image: chosenImage, userId: user.id!, success: { (pictureUrl) in
                    
                    SVProgressHUD.dismiss()
                    if let picture = pictureUrl as String! {
                        user.pictureUrl = picture
                        UserPersistence().persistUser(user)
                    }
                    
                    self.profilePicture.contentMode = .scaleAspectFit
                    self.profilePicture.image = chosenImage
                    
                }, failure: { (error) in
                    SVProgressHUD.dismiss()
                    Utilities.showDefaultNetworkError()
                })
            }
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
