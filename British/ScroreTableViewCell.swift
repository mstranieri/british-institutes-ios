//
//  ScroreTableViewCell.swift
//  British
//
//  Created by MirkoPinna on 20/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class ScroreTableViewCell: UITableViewCell {

    @IBOutlet weak var scorePositionLbl: UILabel!
    @IBOutlet weak var scoreImg: UIImageView!
    @IBOutlet weak var scoreTitleLbl: UILabel!
    @IBOutlet weak var scorePointsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
