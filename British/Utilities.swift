//
//  Utilities.swift
//  British
//
//  Created by MirkoPinna on 12/12/16.
//  Copyright © 2016 effedodici. All rights reserved.
//

import UIKit

class Utilities {
    
    static func showAlertController(_ title: String?,
                                    message: String?,
                                    actions: [UIAlertAction] = [],
                                    style: UIAlertControllerStyle = UIAlertControllerStyle.alert,
                                    presentingViewController: UIViewController? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        var cancelAdded = false
        
        for action in actions {
            alertController.addAction(action)
        }
        
        for action in actions {
            if action.style == .cancel {
                cancelAdded = true
                break
            }
        }
        
        if !cancelAdded {
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in return }))
        }
        
        if let presenter = presentingViewController {
            presenter.present(alertController, animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func showDefaultNetworkError() {
        Utilities.showAlertController("Error", message: "There was an error please try again later.")
    }


}
